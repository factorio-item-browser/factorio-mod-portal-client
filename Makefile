#!make

BUILD_DIR  := build
GO_VERSION := 1.21
SRC_DIRS   := $(shell find . -maxdepth 1 -type d \( -name pkg -o -name internal \) -exec basename {} \; | xargs -I{} echo "./{}/..." | tr '\n' ' ')

.PHONY: help
help: ## Show this help.
	@awk 'BEGIN {FS = ":.*##"; printf "Usage:\033[36m\033[0m\n"} /^[$$()% a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-20s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

all: clean vendor tools fix test lint ## Run all steps for testing the project.

.PHONY: tools
tools: ## Install go tools.
	go install github.com/golangci/golangci-lint/cmd/golangci-lint@latest
	go install github.com/vektra/mockery/v2@latest

.PHONY: generate
generate: ## Generates additional files.
	mockery --config .mockery.yaml

.PHONY: vendor
vendor: ## Build or update the vendor directory.
	go mod tidy -v
	go mod vendor -v

.PHONY: clean
clean: ## Clean the project.
	go clean -v
	rm -rf $(BUILD_DIR) vendor

.PHONY: fix
fix: ## Fix the coding style of the project.
	go fmt $(SRC_DIRS)

.PHONY: test
test: ## Test the project.
	go test -short -race -coverprofile cp.out $(SRC_DIRS)
	go tool cover -func=cp.out
	@rm cp.out

.PHONY: lint
lint: ## Run the linter on the project.
	golangci-lint run --timeout=5m --go=$(GO_VERSION)
