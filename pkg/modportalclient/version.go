package modportalclient

import (
	"fmt"
	"strconv"
	"strings"
)

type Version struct {
	Major uint64
	Minor uint64
	Patch uint64
}

func NewVersion(s string) Version {
	var v Version
	v.parse(s)
	return v
}

func (v *Version) parse(s string) {
	v.Major = 0
	v.Minor = 0
	v.Patch = 0

	parts := strings.Split(s, ".")
	if len(parts) > 0 {
		v.Major, _ = strconv.ParseUint(parts[0], 10, 64)
	}
	if len(parts) > 1 {
		v.Minor, _ = strconv.ParseUint(parts[1], 10, 64)
	}
	if len(parts) > 2 {
		v.Patch, _ = strconv.ParseUint(parts[2], 10, 64)
	}
}

func (v Version) String() string {
	return fmt.Sprintf("%d.%d.%d", v.Major, v.Minor, v.Patch)
}

func (v *Version) UnmarshalText(data []byte) error {
	v.parse(string(data))
	return nil
}

func (v Version) MarshalText() ([]byte, error) {
	return []byte(v.String()), nil
}

// IsZero returns whether the version is the zero value.
func (v Version) IsZero() bool {
	return v.Major == 0 && v.Minor == 0 && v.Patch == 0
}

// CompareTo compares the current version to the provided one. It returns -1 if the current version is smaller, +1 if
// the provided version is smaller (i.e. the current one is bigger), and 0 if both versions are the same.
func (v Version) CompareTo(version Version) int {
	left := v.Major*1000000 + v.Minor*1000 + v.Patch
	right := version.Major*1000000 + version.Minor*1000 + version.Patch

	if left < right {
		return -1
	}
	if left > right {
		return 1
	}
	return 0
}

// CompatibleTo checks whether the provided version is compatible to the current one. This method pays attention to
// 1.0 and 0.18 being actually the same and treats them as compatible.
func (v Version) CompatibleTo(version Version) bool {
	return (v.Major == version.Major && v.Minor == version.Minor) ||
		(v.Major == 0 && v.Minor == 18 && version.Major == 1 && version.Minor == 0) ||
		(v.Major == 1 && v.Minor == 0 && version.Major == 0 && version.Minor == 18)
}
