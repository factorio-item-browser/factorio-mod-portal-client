package modportalclient

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/client"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/request"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/types"
	"gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/test/mocks"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestNew(t *testing.T) {
	instance := New()

	assert.NotNil(t, instance)
	assert.IsType(t, &requestBuilder{}, instance.RequestBuilder())
	assert.NotNil(t, instance.RequestBuilder().(*requestBuilder).factorioBuilder)
	assert.NotNil(t, instance.RequestBuilder().(*requestBuilder).modPortalBuilder)
}

func TestRequestBuilder_Build(t *testing.T) {
	tests := map[string]struct {
		request              types.Request
		expectFactorioBuild  bool
		expectModPortalBuild bool
	}{
		"DownloadModRequest": {
			request:              &DownloadModRequest{},
			expectFactorioBuild:  false,
			expectModPortalBuild: true,
		},
		"FullModRequest": {
			request:              &FullModRequest{},
			expectFactorioBuild:  false,
			expectModPortalBuild: true,
		},
		"LatestFactorioReleasesRequest": {
			request:              &LatestFactorioReleasesRequest{},
			expectFactorioBuild:  true,
			expectModPortalBuild: false,
		},
		"ModListRequest": {
			request:              &ModListRequest{},
			expectFactorioBuild:  false,
			expectModPortalBuild: true,
		},
		"ModRequest": {
			request:              &ModRequest{},
			expectFactorioBuild:  false,
			expectModPortalBuild: true,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			clientRequest := httptest.NewRequest(http.MethodGet, "/test", nil)
			buildError := fmt.Errorf("test error")

			factorioBuilder := mocks.NewRequestBuilder(t)
			if test.expectFactorioBuild {
				factorioBuilder.On("Build", test.request).Once().Return(clientRequest, buildError)
			}

			modPortalBuilder := mocks.NewRequestBuilder(t)
			if test.expectModPortalBuild {
				modPortalBuilder.On("Build", test.request).Once().Return(clientRequest, buildError)
			}

			instance := requestBuilder{
				credentials: factorioCredentials{
					username: "username",
					token:    "token",
				},
				factorioBuilder:  factorioBuilder,
				modPortalBuilder: modPortalBuilder,
			}
			result, err := instance.Build(test.request)

			assert.Equal(t, buildError, err)
			assert.Equal(t, clientRequest, result)
		})
	}
}

func TestWithFactorioApiBaseUrl(t *testing.T) {
	baseUrl := "https://www.example.com"

	builder := requestBuilder{
		factorioBuilder: request.NewBuilder(),
	}
	expectedFactorioBuilder := request.NewBuilder(request.WithBaseUrl(baseUrl))

	instance := New(
		client.WithRequestBuilder(&builder),
		WithFactorioApiBaseUrl(baseUrl),
	)

	assert.NotNil(t, instance)
	assert.Equal(t, expectedFactorioBuilder, instance.RequestBuilder().(*requestBuilder).factorioBuilder)
}

func TestWithModPortalApiBaseUrl(t *testing.T) {
	baseUrl := "https://www.example.com"

	builder := requestBuilder{
		modPortalBuilder: request.NewBuilder(),
	}
	expectedFactorioBuilder := request.NewBuilder(request.WithBaseUrl(baseUrl))

	instance := New(
		client.WithRequestBuilder(&builder),
		WithModPortalApiBaseUrl(baseUrl),
	)

	assert.NotNil(t, instance)
	assert.Equal(t, expectedFactorioBuilder, instance.RequestBuilder().(*requestBuilder).modPortalBuilder)
}

func TestWithFactorioCredentials(t *testing.T) {
	factorioUsername := "username"
	factorioToken := "token"

	builder := requestBuilder{}

	instance := New(
		client.WithRequestBuilder(&builder),
		WithFactorioCredentials(factorioUsername, factorioToken),
	)

	assert.NotNil(t, instance)
	assert.Equal(t, factorioUsername, instance.RequestBuilder().(*requestBuilder).credentials.username)
	assert.Equal(t, factorioToken, instance.RequestBuilder().(*requestBuilder).credentials.token)
}
