package modportalclient

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMod_SelectLatestRelease(t *testing.T) {
	factorioVersion1 := NewVersion("1.2")
	factorioVersion2 := NewVersion("0.1")
	requiredFactorioVersion := NewVersion("1.2.3")

	release1 := Release{
		Version: NewVersion("1.2.3"),
		InfoJson: InfoJson{
			FactorioVersion: &factorioVersion1,
		},
	}
	release2 := Release{
		Version: NewVersion("2.3.4"),
		InfoJson: InfoJson{
			FactorioVersion: &factorioVersion2,
		},
	}

	tests := map[string]struct {
		requiredFactorioVersion *Version
		releases                []Release
		expectedResult          *Release
	}{
		"with release": {
			requiredFactorioVersion: nil,
			releases:                []Release{release1, release2},
			expectedResult:          &release2,
		},
		"without releases": {
			requiredFactorioVersion: nil,
			releases:                []Release{},
			expectedResult:          nil,
		},
		"with factorio version": {
			requiredFactorioVersion: &requiredFactorioVersion,
			releases:                []Release{release1, release2},
			expectedResult:          &release1,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			instance := Mod{
				Releases: test.releases,
			}
			result := instance.SelectLatestRelease(test.requiredFactorioVersion)

			assert.Equal(t, test.expectedResult, result)
		})
	}
}
