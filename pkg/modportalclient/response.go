package modportalclient

import (
	"gitlab.com/blue-psyduck/soushinki.git/pkg/response"
)

type ModResponse struct {
	response.JsonResponse

	Mod
}

type ModListResponse struct {
	response.JsonResponse

	Pagination Pagination `json:"pagination"`
	Results    []Mod      `json:"results"`
}

type LatestFactorioReleasesResponse struct {
	response.JsonResponse

	Experimental LatestRelease `json:"experimental"`
	Stable       LatestRelease `json:"stable"`
}

type LatestRelease struct {
	Alpha    Version `json:"alpha"`
	Demo     Version `json:"demo"`
	Headless Version `json:"headless"`
}
