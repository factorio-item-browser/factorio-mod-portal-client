package modportalclient

import "time"

type Mod struct {
	Category       string    `json:"category"`
	Changelog      string    `json:"changelog"`
	CreatedAt      time.Time `json:"created_at"`
	Description    string    `json:"description"`
	DownloadsCount uint64    `json:"downloads_count"`
	Homepage       string    `json:"homepage"`
	Images         []Image   `json:"images"`
	LatestRelease  *Release  `json:"latest_release"`
	License        License   `json:"license"`
	Name           string    `json:"name"`
	Owner          string    `json:"owner"`
	Releases       []Release `json:"releases"`
	Score          float64   `json:"score"`
	SourceUrl      string    `json:"source_url"`
	Summary        string    `json:"summary"`
	Tags           []string  `json:"tags"`
	Thumbnail      string    `json:"thumbnail"`
	Title          string    `json:"title"`
	UpdatedAt      time.Time `json:"updated_at"`
}

// SelectLatestRelease selects the latest release from the Releases list.
func (m *Mod) SelectLatestRelease(requiredFactorioVersion *Version) *Release {
	var result *Release

	for key, release := range m.Releases {
		if requiredFactorioVersion != nil && release.InfoJson.FactorioVersion != nil &&
			!requiredFactorioVersion.CompatibleTo(*release.InfoJson.FactorioVersion) {
			// Release version is not compatible to the required Factorio version, so skip it.
			continue
		}

		if result == nil || release.Version.CompareTo(result.Version) > 0 {
			result = &m.Releases[key]
		}
	}

	return result
}

type Image struct {
	Id        string `json:"id"`
	Thumbnail string `json:"thumbnail"`
	Url       string `json:"url"`
}

type License struct {
	Description string `json:"description"`
	Id          string `json:"id"`
	Name        string `json:"name"`
	Title       string `json:"title"`
	Url         string `json:"url"`
}

type Release struct {
	DownloadUrl string    `json:"download_url"`
	FileName    string    `json:"file_name"`
	InfoJson    InfoJson  `json:"info_json"`
	ReleasedAt  time.Time `json:"released_at"`
	Sha1        string    `json:"sha_1"`
	Version     Version   `json:"version"`
}

type InfoJson struct {
	FactorioVersion *Version     `json:"factorio_version"`
	Dependencies    []Dependency `json:"dependencies"`
}

type Pagination struct {
	Count     uint64          `json:"count"`
	Links     PaginationLinks `json:"links"`
	Page      uint64          `json:"page"`
	PageCount uint64          `json:"page_count"`
	PageSize  uint64          `json:"page_size"`
}

type PaginationLinks struct {
	First string `json:"first"`
	Prev  string `json:"prev"`
	Next  string `json:"next"`
	Last  string `json:"last"`
}
