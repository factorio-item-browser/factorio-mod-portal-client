package modportalclient

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewDependency(t *testing.T) {
	s := "? abc >= 1.2.3"
	expectedResult := Dependency{
		Type:     DependencyTypeOptional,
		ModName:  "abc",
		Operator: DependencyOperatorGreaterThanOrEqual,
		Version:  &Version{1, 2, 3},
	}

	result := NewDependency(s)
	assert.Equal(t, expectedResult, result)
}

func TestDependency_parse(t *testing.T) {
	createVersion := func(version string) *Version {
		result := NewVersion(version)
		return &result
	}

	tests := []struct {
		dependency       string
		expectedType     DependencyType
		expectedModName  string
		expectedOperator DependencyOperator
		expectedVersion  *Version
	}{
		{"abc", DependencyTypeMandatory, "abc", DependencyOperatorAny, nil},
		{"abc = 1.0", DependencyTypeMandatory, "abc", DependencyOperatorEqual, createVersion("1.0.0")},
		{"abc > 1.0", DependencyTypeMandatory, "abc", DependencyOperatorGreaterThan, createVersion("1.0.0")},
		{"abc >= 1.0", DependencyTypeMandatory, "abc", DependencyOperatorGreaterThanOrEqual, createVersion("1.0.0")},
		{"abc < 1.0", DependencyTypeMandatory, "abc", DependencyOperatorLessThan, createVersion("1.0.0")},
		{"abc <= 1.0", DependencyTypeMandatory, "abc", DependencyOperatorLessThanOrEqual, createVersion("1.0.0")},

		{"? abc", DependencyTypeOptional, "abc", DependencyOperatorAny, nil},
		{"? abc = 1.0", DependencyTypeOptional, "abc", DependencyOperatorEqual, createVersion("1.0.0")},
		{"? abc > 1.0", DependencyTypeOptional, "abc", DependencyOperatorGreaterThan, createVersion("1.0.0")},
		{"? abc >= 1.0", DependencyTypeOptional, "abc", DependencyOperatorGreaterThanOrEqual, createVersion("1.0.0")},
		{"? abc < 1.0", DependencyTypeOptional, "abc", DependencyOperatorLessThan, createVersion("1.0.0")},
		{"? abc <= 1.0", DependencyTypeOptional, "abc", DependencyOperatorLessThanOrEqual, createVersion("1.0.0")},

		{"(?) abc", DependencyTypeOptionalHidden, "abc", DependencyOperatorAny, nil},
		{"(?) abc = 1.0", DependencyTypeOptionalHidden, "abc", DependencyOperatorEqual, createVersion("1.0.0")},
		{"(?) abc > 1.0", DependencyTypeOptionalHidden, "abc", DependencyOperatorGreaterThan, createVersion("1.0.0")},
		{"(?) abc >= 1.0", DependencyTypeOptionalHidden, "abc", DependencyOperatorGreaterThanOrEqual, createVersion("1.0.0")},
		{"(?) abc < 1.0", DependencyTypeOptionalHidden, "abc", DependencyOperatorLessThan, createVersion("1.0.0")},
		{"(?) abc <= 1.0", DependencyTypeOptionalHidden, "abc", DependencyOperatorLessThanOrEqual, createVersion("1.0.0")},

		{"! abc", DependencyTypeConflict, "abc", DependencyOperatorAny, nil},
		{"! abc = 1.0", DependencyTypeConflict, "abc", DependencyOperatorEqual, createVersion("1.0.0")},
		{"! abc > 1.0", DependencyTypeConflict, "abc", DependencyOperatorGreaterThan, createVersion("1.0.0")},
		{"! abc >= 1.0", DependencyTypeConflict, "abc", DependencyOperatorGreaterThanOrEqual, createVersion("1.0.0")},
		{"! abc < 1.0", DependencyTypeConflict, "abc", DependencyOperatorLessThan, createVersion("1.0.0")},
		{"! abc <= 1.0", DependencyTypeConflict, "abc", DependencyOperatorLessThanOrEqual, createVersion("1.0.0")},

		// Optional spaces
		{"abc>1.0", DependencyTypeMandatory, "abc", DependencyOperatorGreaterThan, createVersion("1.0.0")},
		{"~abc>1.0", DependencyTypeMandatoryCircular, "abc", DependencyOperatorGreaterThan, createVersion("1.0.0")},
		{"?abc>=1.0", DependencyTypeOptional, "abc", DependencyOperatorGreaterThanOrEqual, createVersion("1.0.0")},
		{"(?)abc<1.0", DependencyTypeOptionalHidden, "abc", DependencyOperatorLessThan, createVersion("1.0.0")},
		{"!abc<=1.0", DependencyTypeConflict, "abc", DependencyOperatorLessThanOrEqual, createVersion("1.0.0")},

		// More complex mod names
		{"abc-def > 1.0.0", DependencyTypeMandatory, "abc-def", DependencyOperatorGreaterThan, createVersion("1.0.0")},
		{"abc_def > 1.0.0", DependencyTypeMandatory, "abc_def", DependencyOperatorGreaterThan, createVersion("1.0.0")},
		{"Abc Def > 1.0.0", DependencyTypeMandatory, "Abc Def", DependencyOperatorGreaterThan, createVersion("1.0.0")},

		// Invalid
		{"", DependencyTypeMandatory, "", DependencyOperatorAny, nil},
	}

	for _, test := range tests {
		t.Run(test.dependency, func(t *testing.T) {
			var instance Dependency
			instance.parse(test.dependency)

			assert.Equal(t, test.expectedType, instance.Type)
			assert.Equal(t, test.expectedModName, instance.ModName)
			assert.Equal(t, test.expectedOperator, instance.Operator)
			assert.Equal(t, test.expectedVersion, instance.Version)
		})
	}
}

func TestDependency_String(t *testing.T) {
	tests := map[string]struct {
		dependencyType     DependencyType
		modName            string
		dependencyOperator DependencyOperator
		version            *Version
		expectedResult     string
	}{
		"with version": {
			dependencyType:     DependencyTypeOptional,
			modName:            "abc",
			dependencyOperator: DependencyOperatorGreaterThanOrEqual,
			version:            &Version{1, 2, 3},
			expectedResult:     "? abc >= 1.2.3",
		},
		"with any": {
			dependencyType:     DependencyTypeOptional,
			modName:            "abc",
			dependencyOperator: DependencyOperatorAny,
			version:            &Version{1, 2, 3},
			expectedResult:     "? abc",
		},
		"without version": {
			dependencyType:     DependencyTypeOptional,
			modName:            "abc",
			dependencyOperator: DependencyOperatorGreaterThanOrEqual,
			version:            nil,
			expectedResult:     "? abc",
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			instance := Dependency{
				Type:     test.dependencyType,
				ModName:  test.modName,
				Operator: test.dependencyOperator,
				Version:  test.version,
			}
			result := instance.String()

			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestDependency_UnmarshalText(t *testing.T) {
	type test struct {
		Dependency Dependency `json:"dependency"`
	}

	data := []byte(`{"dependency":"? abc >= 1.2.3"}`)
	expectedInstance := test{
		Dependency: Dependency{
			Type:     DependencyTypeOptional,
			ModName:  "abc",
			Operator: DependencyOperatorGreaterThanOrEqual,
			Version:  &Version{1, 2, 3},
		},
	}

	var instance test
	err := json.Unmarshal(data, &instance)

	assert.Nil(t, err)
	assert.Equal(t, expectedInstance, instance)
}

func TestDependency_MarshalText(t *testing.T) {
	type test struct {
		Dependency Dependency `json:"dependency"`
	}

	instance := test{
		Dependency: Dependency{
			Type:     DependencyTypeOptional,
			ModName:  "abc",
			Operator: DependencyOperatorGreaterThanOrEqual,
			Version:  &Version{1, 2, 3},
		},
	}
	expectedResult := []byte(`{"dependency":"? abc \u003e= 1.2.3"}`)

	result, err := json.Marshal(instance)

	assert.Nil(t, err)
	assert.Equal(t, expectedResult, result)
}

func TestDependency_MatchesVersion(t *testing.T) {
	version := &Version{1, 2, 3}

	tests := map[string]struct {
		dependencyOperator DependencyOperator
		dependencyVersion  *Version
		version            Version
		expectedResult     bool
	}{
		"any 1.2.3":  {DependencyOperatorAny, nil, Version{1, 2, 3}, true},
		"impossible": {DependencyOperatorAny, version, Version{1, 2, 3}, true},

		"mod > 1.2.3, 2.0.0":  {DependencyOperatorGreaterThan, version, Version{2, 0, 0}, true},
		"mod > 1.2.3, 1.3.0":  {DependencyOperatorGreaterThan, version, Version{1, 3, 0}, true},
		"mod > 1.2.3, 1.2.4":  {DependencyOperatorGreaterThan, version, Version{1, 2, 4}, true},
		"mod > 1.2.3, 1.2.3":  {DependencyOperatorGreaterThan, version, Version{1, 2, 3}, false},
		"mod > 1.2.3, 1.2.2":  {DependencyOperatorGreaterThan, version, Version{1, 2, 2}, false},
		"mod > 1.2.3, 1.1.0":  {DependencyOperatorGreaterThan, version, Version{1, 1, 0}, false},
		"mod > 1.2.3, 0.18.0": {DependencyOperatorGreaterThan, version, Version{0, 18, 0}, false},

		"mod >= 1.2.3, 2.0.0":  {DependencyOperatorGreaterThanOrEqual, version, Version{2, 0, 0}, true},
		"mod >= 1.2.3, 1.3.0":  {DependencyOperatorGreaterThanOrEqual, version, Version{1, 3, 0}, true},
		"mod >= 1.2.3, 1.2.4":  {DependencyOperatorGreaterThanOrEqual, version, Version{1, 2, 4}, true},
		"mod >= 1.2.3, 1.2.3":  {DependencyOperatorGreaterThanOrEqual, version, Version{1, 2, 3}, true},
		"mod >= 1.2.3, 1.2.2":  {DependencyOperatorGreaterThanOrEqual, version, Version{1, 2, 2}, false},
		"mod >= 1.2.3, 1.1.0":  {DependencyOperatorGreaterThanOrEqual, version, Version{1, 1, 0}, false},
		"mod >= 1.2.3, 0.18.0": {DependencyOperatorGreaterThanOrEqual, version, Version{0, 18, 0}, false},

		"mod = 1.2.3, 2.0.0":  {DependencyOperatorEqual, version, Version{2, 0, 0}, false},
		"mod = 1.2.3, 1.3.0":  {DependencyOperatorEqual, version, Version{1, 3, 0}, false},
		"mod = 1.2.3, 1.2.4":  {DependencyOperatorEqual, version, Version{1, 2, 4}, false},
		"mod = 1.2.3, 1.2.3":  {DependencyOperatorEqual, version, Version{1, 2, 3}, true},
		"mod = 1.2.3, 1.2.2":  {DependencyOperatorEqual, version, Version{1, 2, 2}, false},
		"mod = 1.2.3, 1.1.0":  {DependencyOperatorEqual, version, Version{1, 1, 0}, false},
		"mod = 1.2.3, 0.18.0": {DependencyOperatorEqual, version, Version{0, 18, 0}, false},

		"mod <= 1.2.3, 2.0.0":  {DependencyOperatorLessThanOrEqual, version, Version{2, 0, 0}, false},
		"mod <= 1.2.3, 1.3.0":  {DependencyOperatorLessThanOrEqual, version, Version{1, 3, 0}, false},
		"mod <= 1.2.3, 1.2.4":  {DependencyOperatorLessThanOrEqual, version, Version{1, 2, 4}, false},
		"mod <= 1.2.3, 1.2.3":  {DependencyOperatorLessThanOrEqual, version, Version{1, 2, 3}, true},
		"mod <= 1.2.3, 1.2.2":  {DependencyOperatorLessThanOrEqual, version, Version{1, 2, 2}, true},
		"mod <= 1.2.3, 1.1.0":  {DependencyOperatorLessThanOrEqual, version, Version{1, 1, 0}, true},
		"mod <= 1.2.3, 0.18.0": {DependencyOperatorLessThanOrEqual, version, Version{0, 18, 0}, true},

		"mod < 1.2.3, 2.0.0":  {DependencyOperatorLessThan, version, Version{2, 0, 0}, false},
		"mod < 1.2.3, 1.3.0":  {DependencyOperatorLessThan, version, Version{1, 3, 0}, false},
		"mod < 1.2.3, 1.2.4":  {DependencyOperatorLessThan, version, Version{1, 2, 4}, false},
		"mod < 1.2.3, 1.2.3":  {DependencyOperatorLessThan, version, Version{1, 2, 3}, false},
		"mod < 1.2.3, 1.2.2":  {DependencyOperatorLessThan, version, Version{1, 2, 2}, true},
		"mod < 1.2.3, 1.1.0":  {DependencyOperatorLessThan, version, Version{1, 1, 0}, true},
		"mod < 1.2.3, 0.18.0": {DependencyOperatorLessThan, version, Version{0, 18, 0}, true},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			instance := Dependency{
				Operator: test.dependencyOperator,
				Version:  test.dependencyVersion,
			}
			result := instance.MatchesVersion(test.version)

			assert.Equal(t, test.expectedResult, result)
		})
	}
}
