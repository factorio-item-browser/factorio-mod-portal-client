package modportalclient

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestModRequest_Method(t *testing.T) {
	request := ModRequest{}
	result, err := request.Method()

	assert.Nil(t, err)
	assert.Equal(t, http.MethodGet, result)
}

func TestModRequest_Path(t *testing.T) {
	modName := "abc"
	expectedResult := "/api/mods/abc"

	request := ModRequest{
		ModName: modName,
	}
	result, err := request.Path()

	assert.Nil(t, err)
	assert.Equal(t, expectedResult, result)
}

func TestFullModRequest_Method(t *testing.T) {
	request := FullModRequest{}
	result, err := request.Method()

	assert.Nil(t, err)
	assert.Equal(t, http.MethodGet, result)
}

func TestFullModRequest_Path(t *testing.T) {
	modName := "abc"
	expectedResult := "/api/mods/abc/full"

	request := FullModRequest{
		ModName: modName,
	}
	result, err := request.Path()

	assert.Nil(t, err)
	assert.Equal(t, expectedResult, result)
}

func TestModListRequest_Method(t *testing.T) {
	request := ModListRequest{}
	result, err := request.Method()

	assert.Nil(t, err)
	assert.Equal(t, http.MethodGet, result)
}

func TestModListRequest_Path(t *testing.T) {
	tests := map[string]struct {
		request        ModListRequest
		expectedResult string
	}{
		"with all": {
			request: ModListRequest{
				HideDeprecated: true,
				Page:           7,
				PageSize:       42,
				Sort:           SortName,
				SortOrder:      SortOrderDesc,
				Names:          []string{"abc", "def"},
				Version:        NewVersion("1.2.3"),
			},
			expectedResult: "/api/mods?hide_deprecated=true&namelist=abc%2Cdef&page=7&page_size=42&sort=name&sort_order=desc&version=1.2.3",
		},
		"with max page size": {
			request: ModListRequest{
				HideDeprecated: true,
				Page:           7,
				PageSize:       PageSizeMax,
				Sort:           SortName,
				SortOrder:      SortOrderDesc,
				Names:          []string{"abc", "def"},
				Version:        NewVersion("1.2.3"),
			},
			expectedResult: "/api/mods?hide_deprecated=true&namelist=abc%2Cdef&page=7&page_size=max&sort=name&sort_order=desc&version=1.2.3",
		},
		"with none": {
			request:        ModListRequest{},
			expectedResult: "/api/mods",
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			result, err := test.request.Path()

			assert.Nil(t, err)
			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestDownloadModRequest_Method(t *testing.T) {
	request := DownloadModRequest{}
	result, err := request.Method()

	assert.Nil(t, err)
	assert.Equal(t, http.MethodGet, result)
}

func TestDownloadModRequest_Path(t *testing.T) {
	downloadUrl := "/download/thing"
	credentials := factorioCredentials{
		username: "foo",
		token:    "bar",
	}
	expectedResult := "/download/thing?token=bar&username=foo"

	instance := DownloadModRequest{
		DownloadUrl: downloadUrl,
	}
	instance.setFactorioCredentials(credentials)
	result, err := instance.Path()

	assert.Nil(t, err)
	assert.Equal(t, expectedResult, result)
}

func TestLatestFactorioReleasesRequest_Method(t *testing.T) {
	request := LatestFactorioReleasesRequest{}
	result, err := request.Method()

	assert.Nil(t, err)
	assert.Equal(t, http.MethodGet, result)
}

func TestLatestFactorioReleasesRequest_Path(t *testing.T) {
	expectedResult := "/api/latest-releases"

	request := LatestFactorioReleasesRequest{}
	result, err := request.Path()

	assert.Nil(t, err)
	assert.Equal(t, expectedResult, result)
}
