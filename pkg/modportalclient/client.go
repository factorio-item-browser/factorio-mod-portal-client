package modportalclient

import (
	"gitlab.com/blue-psyduck/soushinki.git/pkg/client"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/request"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/types"
	"net/http"
)

const (
	defaultFactorioApiBaseUrl  = "https://www.factorio.com"
	defaultModPortalApiBaseUrl = "https://mods.factorio.com"
)

type factorioCredentials struct {
	username string
	token    string
}

var WithHttpClient = client.WithHttpClient

// New creates a new instance of the client to send requests to the Factorio Mod Portal or the Factorio API.
// The client has useful default values, so it will work out of the box without providing any options.
// When a request requiring Factorio credentials is used (mostly for downloading files), then WithFactorioCredentials()
// must be passed to this function. If you want to use a custom HTTP client, pass WithHttpClient() to this function.
func New(options ...client.Option) *client.Client {
	options = append(
		[]client.Option{
			client.WithName("factorio-mod-portal"),
			client.WithRequestBuilder(newRequestBuilder()),
		},
		options...,
	)

	return client.New(options...)
}

type requestBuilder struct {
	credentials      factorioCredentials
	factorioBuilder  types.RequestBuilder
	modPortalBuilder types.RequestBuilder
}

func newRequestBuilder() *requestBuilder {
	factorioBuilder := request.NewBuilder(request.WithBaseUrl(defaultFactorioApiBaseUrl))
	modPortalBuilder := request.NewBuilder(request.WithBaseUrl(defaultModPortalApiBaseUrl))

	return &requestBuilder{
		factorioBuilder:  factorioBuilder,
		modPortalBuilder: modPortalBuilder,
	}
}

type credentialsSetter interface {
	setFactorioCredentials(credentials factorioCredentials)
}

func (b *requestBuilder) Build(req types.Request) (*http.Request, error) {
	if fs, ok := req.(credentialsSetter); ok {
		fs.setFactorioCredentials(b.credentials)
	}

	var builder types.RequestBuilder
	switch req.(type) {
	case *LatestFactorioReleasesRequest:
		builder = b.factorioBuilder
	default:
		builder = b.modPortalBuilder
	}

	return builder.Build(req)
}

// WithFactorioApiBaseUrl sets the base url to be used for the Factorio API. The default value is
// "https://www.factorio.com".
func WithFactorioApiBaseUrl(factorioApiBaseUrl string) client.Option {
	return func(c *client.Client) {
		rb, ok := c.RequestBuilder().(*requestBuilder)
		if ok {
			request.WithBaseUrl(factorioApiBaseUrl)(rb.factorioBuilder.(*request.Builder))
		}
	}
}

// WithModPortalApiBaseUrl sets the base url to the Factorio Mod Portal API. The default value is
// "https://mods.factorio.com".
func WithModPortalApiBaseUrl(modPortalApiBaseUrl string) client.Option {
	return func(c *client.Client) {
		rb, ok := c.RequestBuilder().(*requestBuilder)
		if ok {
			request.WithBaseUrl(modPortalApiBaseUrl)(rb.modPortalBuilder.(*request.Builder))
		}
	}
}

// WithFactorioCredentials sets the credentials required for some of the requests, especially those downloading files
// from the API. factorioUsername is the username to use, the factorioToken can be generated and viewed on the Factorio
// page. Not setting the credentials will lead to 403 errors on the requests which require them.
func WithFactorioCredentials(factorioUsername string, factorioToken string) client.Option {
	return func(c *client.Client) {
		rb, ok := c.RequestBuilder().(*requestBuilder)
		if ok {
			rb.credentials = factorioCredentials{
				username: factorioUsername,
				token:    factorioToken,
			}
		}
	}
}
