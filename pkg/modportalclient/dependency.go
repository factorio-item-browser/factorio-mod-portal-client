package modportalclient

import (
	"fmt"
	"regexp"
	"strings"
)

var regexpDependency = regexp.MustCompile(`^(~|\?|!|\(\?\))? *(.+?) *(?:(<|<=|=|>=|>) *([\d.]+))?$`)

type DependencyType string
type DependencyOperator string

const (
	DependencyTypeMandatory         = ""
	DependencyTypeMandatoryCircular = "~"
	DependencyTypeOptional          = "?"
	DependencyTypeOptionalHidden    = "(?)"
	DependencyTypeConflict          = "!"

	DependencyOperatorAny                = ""
	DependencyOperatorEqual              = "="
	DependencyOperatorGreaterThan        = ">"
	DependencyOperatorGreaterThanOrEqual = ">="
	DependencyOperatorLessThan           = "<"
	DependencyOperatorLessThanOrEqual    = "<="
)

type Dependency struct {
	Type     DependencyType
	ModName  string
	Operator DependencyOperator
	Version  *Version
}

func NewDependency(s string) Dependency {
	var d Dependency
	d.parse(s)
	return d
}

func (d *Dependency) parse(s string) {
	matches := regexpDependency.FindStringSubmatch(s)
	if matches == nil {
		return
	}

	d.Type = DependencyType(matches[1])
	d.ModName = matches[2]
	d.Operator = DependencyOperator(matches[3])
	if matches[4] != "" {
		version := NewVersion(matches[4])
		d.Version = &version
	}
}

func (d Dependency) String() string {
	result := strings.TrimSpace(fmt.Sprintf("%s %s", d.Type, d.ModName))
	if d.Operator != "" && d.Version != nil {
		result += fmt.Sprintf(" %s %s", d.Operator, d.Version.String())
	}
	return result
}

func (d *Dependency) UnmarshalText(data []byte) error {
	d.parse(string(data))
	return nil
}

func (d Dependency) MarshalText() ([]byte, error) {
	return []byte(d.String()), nil
}

// MatchesVersion returns whether the dependency is fulfilled by the provided version.
func (d Dependency) MatchesVersion(v Version) bool {
	if d.Version == nil {
		return true
	}

	switch d.Operator {
	case DependencyOperatorGreaterThan:
		return v.CompareTo(*d.Version) > 0
	case DependencyOperatorGreaterThanOrEqual:
		return v.CompareTo(*d.Version) >= 0
	case DependencyOperatorEqual:
		return v.CompareTo(*d.Version) == 0
	case DependencyOperatorLessThanOrEqual:
		return v.CompareTo(*d.Version) <= 0
	case DependencyOperatorLessThan:
		return v.CompareTo(*d.Version) < 0
	}

	return true
}
