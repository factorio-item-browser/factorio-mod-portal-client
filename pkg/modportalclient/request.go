package modportalclient

import (
	"fmt"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/request"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/response"
	"math"
	"net/http"
	"net/url"
	"strings"
)

// ModRequest is the request for fetching the basic data of a single mod, specified by its name.
type ModRequest struct {
	request.ResponseCreator[*ModResponse]

	// ModName is the name of the mod to fetch the data of.
	ModName string
}

func (r *ModRequest) Method() (string, error) {
	return http.MethodGet, nil
}

func (r *ModRequest) Path() (string, error) {
	return fmt.Sprintf("/api/mods/%s", r.ModName), nil
}

// FullModRequest is the request for fetching all details of a single mod, specified by its name.
type FullModRequest struct {
	request.ResponseCreator[*ModResponse]

	// ModName is the name of the mod to fetch the data of.
	ModName string
}

func (r *FullModRequest) Method() (string, error) {
	return http.MethodGet, nil
}

func (r *FullModRequest) Path() (string, error) {
	return fmt.Sprintf("/api/mods/%s/full", r.ModName), nil
}

type Sort string
type SortOrder string

const (
	SortName      Sort = "name"
	SortCreatedAt Sort = "created_at"
	SortUpdatedAt Sort = "updated_at"

	SortOrderAsc  SortOrder = "asc"
	SortOrderDesc SortOrder = "desc"

	PageSizeMax uint64 = math.MaxUint64
)

// ModListRequest is the request for fetching the data of a list of mods, filtered by several criteria.
type ModListRequest struct {
	request.ResponseCreator[*ModListResponse]

	// HideDeprecated specifies whether deprecated mods should be removed from the result list.
	HideDeprecated bool
	// Page specifies the page of the results to return.
	Page uint64
	// PageSize specifies the size of the page to return from the results. Set to PageSizeMax for the maximal possible
	// page size.
	PageSize uint64
	// Sort is the sorting criteria to use for the results.
	Sort Sort
	// SortOrder is the order to return the sorted results in.
	SortOrder SortOrder
	// Names is the list of mod names to return.
	Names []string
	// Version is the version of Factorio the returned mods must be compatible to.
	Version Version
}

func (r *ModListRequest) Method() (string, error) {
	return http.MethodGet, nil
}

func (r *ModListRequest) Path() (string, error) {
	params := url.Values{}
	if r.HideDeprecated {
		params.Set("hide_deprecated", "true")
	}
	if r.Page > 0 {
		params.Set("page", fmt.Sprintf("%d", r.Page))
	}
	if r.PageSize > 0 {
		if r.PageSize == PageSizeMax {
			params.Set("page_size", "max")
		} else {
			params.Set("page_size", fmt.Sprintf("%d", r.PageSize))
		}
	}
	if r.Sort != "" {
		params.Set("sort", string(r.Sort))
	}
	if r.SortOrder != "" {
		params.Set("sort_order", string(r.SortOrder))
	}
	if len(r.Names) > 0 {
		params.Set("namelist", strings.Join(r.Names, ","))
	}
	if !r.Version.IsZero() {
		params.Set("version", r.Version.String())
	}

	return strings.TrimRight(fmt.Sprintf("/api/mods?%s", params.Encode()), "?"), nil
}

// DownloadModRequest is the request to actually download a mod file from the API. This request requires the Factorio
// credentials to be set in the client.
type DownloadModRequest struct {
	request.ResponseCreator[*response.RawResponse]
	credentials factorioCredentials

	// DownloadUrl is the download URL as provided in the release node of a mod.
	DownloadUrl string
}

func (r *DownloadModRequest) setFactorioCredentials(credentials factorioCredentials) {
	r.credentials = credentials
}

func (r *DownloadModRequest) Method() (string, error) {
	return http.MethodGet, nil
}

func (r *DownloadModRequest) Path() (string, error) {
	params := url.Values{}
	params.Set("username", r.credentials.username)
	params.Set("token", r.credentials.token)

	return strings.TrimRight(fmt.Sprintf("%s?%s", r.DownloadUrl, params.Encode()), "?"), nil
}

// LatestFactorioReleasesRequest is the request for fetching the currently latest versions of Factorio. No additional
// parameters are required, and the result will contain the most recent versions of the full game, the headless version,
// and the demo, for both stable and experimental.
type LatestFactorioReleasesRequest struct {
	request.ResponseCreator[*LatestFactorioReleasesResponse]
}

func (r *LatestFactorioReleasesRequest) Method() (string, error) {
	return http.MethodGet, nil
}

func (r *LatestFactorioReleasesRequest) Path() (string, error) {
	return "/api/latest-releases", nil
}
