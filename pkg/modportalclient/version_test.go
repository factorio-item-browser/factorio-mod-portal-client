package modportalclient

import (
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewVersion(t *testing.T) {
	s := "1.2.3"
	expectedResult := Version{
		Major: 1,
		Minor: 2,
		Patch: 3,
	}

	result := NewVersion(s)
	assert.Equal(t, expectedResult, result)
}

func TestVersion_parse(t *testing.T) {
	tests := []struct {
		version string
		major   uint64
		minor   uint64
		patch   uint64
	}{
		{
			version: "1.2.3",
			major:   1,
			minor:   2,
			patch:   3,
		},
		{
			version: "0.1.2",
			major:   0,
			minor:   1,
			patch:   2,
		},
		{
			version: "13.37",
			major:   13,
			minor:   37,
			patch:   0,
		},
		{
			version: "42",
			major:   42,
			minor:   0,
			patch:   0,
		},
	}

	for _, test := range tests {
		t.Run(test.version, func(t *testing.T) {
			var instance Version
			instance.parse(test.version)

			assert.Equal(t, test.major, instance.Major)
			assert.Equal(t, test.minor, instance.Minor)
			assert.Equal(t, test.patch, instance.Patch)
		})
	}
}

func TestVersion_String(t *testing.T) {
	expectedResult := "1.2.3"

	instance := Version{
		Major: 1,
		Minor: 2,
		Patch: 3,
	}
	result := instance.String()

	assert.Equal(t, expectedResult, result)
}

func TestVersion_MarshalText(t *testing.T) {
	type test struct {
		Version Version `json:"version"`
	}

	instance := test{
		Version: Version{
			Major: 1,
			Minor: 2,
			Patch: 3,
		},
	}
	expectedResult := []byte(`{"version":"1.2.3"}`)

	result, err := json.Marshal(instance)

	assert.Nil(t, err)
	assert.Equal(t, expectedResult, result)
}

func TestVersion_UnmarshalText(t *testing.T) {
	type test struct {
		Version Version `json:"version"`
	}

	data := []byte(`{"version":"1.2.3"}`)
	expectedInstance := test{
		Version: Version{
			Major: 1,
			Minor: 2,
			Patch: 3,
		},
	}

	var instance test
	err := json.Unmarshal(data, &instance)

	assert.Nil(t, err)
	assert.Equal(t, expectedInstance, instance)
}

func TestVersion_IsZero(t *testing.T) {
	tests := map[string]struct {
		version        string
		expectedResult bool
	}{
		"zero": {
			version:        "0.0.0",
			expectedResult: true,
		},
		"major not zero": {
			version:        "1.0.0",
			expectedResult: false,
		},
		"minor not zero": {
			version:        "0.1.0",
			expectedResult: false,
		},
		"patch not zero": {
			version:        "0.0.1",
			expectedResult: false,
		},
		"all not zero": {
			version:        "1.2.3",
			expectedResult: false,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			instance := NewVersion(test.version)
			result := instance.IsZero()

			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestVersion_CompareTo(t *testing.T) {
	tests := []struct {
		left           string
		right          string
		expectedResult int
	}{
		{
			left:           "1.2.3",
			right:          "2.3.4",
			expectedResult: -1,
		},
		{
			left:           "1.2.3",
			right:          "1.2.3",
			expectedResult: 0,
		},
		{
			left:           "1.2.3",
			right:          "0.1.2",
			expectedResult: 1,
		},
		{
			left:           "1.2.3",
			right:          "1.2.4",
			expectedResult: -1,
		},
		{
			left:           "1.2.3",
			right:          "1.2.2",
			expectedResult: 1,
		},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%s <=> %s", test.left, test.right), func(t *testing.T) {
			left := NewVersion(test.left)
			right := NewVersion(test.right)

			result := left.CompareTo(right)

			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestVersion_CompatibleTo(t *testing.T) {
	tests := []struct {
		left           string
		right          string
		expectedResult bool
	}{
		{
			left:           "1.2.3",
			right:          "1.2.3",
			expectedResult: true,
		},
		{
			left:           "1.2.3",
			right:          "1.2.4",
			expectedResult: true,
		},
		{
			left:           "1.2.3",
			right:          "1.2.2",
			expectedResult: true,
		},
		{
			left:           "1.2.3",
			right:          "1.3.3",
			expectedResult: false,
		},
		{
			left:           "1.2.3",
			right:          "1.1.3",
			expectedResult: false,
		},

		{
			left:           "1.0.2",
			right:          "0.18.36",
			expectedResult: true,
		},
		{
			left:           "1.1.0",
			right:          "1.18.36",
			expectedResult: false,
		},
		{
			left:           "0.18.36",
			right:          "1.0.2",
			expectedResult: true,
		},
		{
			left:           "0.17.0",
			right:          "1.0.0",
			expectedResult: false,
		},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%s to %s", test.left, test.right), func(t *testing.T) {
			instance := NewVersion(test.left)
			result := instance.CompatibleTo(NewVersion(test.right))

			assert.Equal(t, test.expectedResult, result)
		})
	}
}
