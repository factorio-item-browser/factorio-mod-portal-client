# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v1.0.1]

### Changed

- Go version to 1.21.

### Fixed

- Factorio credentials being shared between all client instances.

## [v1.0.0] - 2023-10-23

### Added

- Initial release of the package.

[Unreleased]: https://gitlab.com/factorio-item-browser/factorio-mod-portal-client/-/compare/v1.0.1...HEAD
[v1.0.1]: https://gitlab.com/factorio-item-browser/factorio-mod-portal-client/-/compare/v1.0.0...v1.0.1
[v1.0.0]: https://gitlab.com/factorio-item-browser/factorio-mod-portal-client/-/tags/v1.0.0