# Factorio Mod Portal Client

This Go package implements a client for accessing the Factorio API and the Factorio Mod Portal API, based on the
[Soushinki package](https://gitlab.com/blue-psyduck/soushinki).

## Implemented requests

### Factorio API

This package implements the following requests of the general [Factorio API](https://wiki.factorio.com/Download_API).

| Endpoint            | Request struct                | Response struct                | Requires credentials | 
|---------------------|-------------------------------|--------------------------------|----------------------|
| /api/latest-release | LatestFactorioReleasesRequest | LatestFactorioReleasesResponse | No                   | 

### Factorio Mod Portal API

This package also implements most of the requests against the [Factorio Mod Portal API](https://wiki.factorio.com/Mod_portal_API).

| Endpoint                  | Request struct     | Response struct | Requires credentials | 
|---------------------------|--------------------|-----------------|----------------------|
| /api/mods/{mod-name}      | ModRequest         | ModResponse     | No                   |
| /api/mods/{mod-name}/full | FullModRequest     | ModResponse     | No                   |
| /api/mods                 | ModListRequest     | ModListResponse | No                   |
|                           | DownloadModRequest | RawResponse     | Yes                  | 

## Installation

```bash
go get gitlab.com/factorio-item-browser/factorio-mod-portal-client
```

## Usage

First you have to create and configure the client:

```go
package main

import (
	"gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
)

func main() {
	// Create the client instance
	client := modportalclient.New(
		// Credentials are required to actually download any files from the API.
		WithFactorioCredentials("your-factorio-username", "your-factorio-token"),
	)
}
```

Providing the credentials is optional. If you use a request requiring them (see tables above), you must provide them, 
though.

Once the client is created, you can send your requests to the API and cast the received response like the following:

```go
	// Example request: Download data of the desired mod
	modRequest := ModRequest{
		ModName: "textplates",
	}
	modResponse, err := modRequest.CastResponse(client.Send(context.TODO(), &modRequest))
	if err != nil {
		panic(err)
	}
	
	// Do something with the received response
	fmt.Printf("Mod author: %s\n", modResponse.Owner)
```

You can also find full examples in the [example](example) directory of the repository.