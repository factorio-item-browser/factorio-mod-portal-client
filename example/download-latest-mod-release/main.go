package main

import (
	"context"
	. "gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
	"os"
)

func main() {
	ctx := context.Background()

	// Create the client instance
	client := New(
		// Credentials are required to actually download any files from the API.
		WithFactorioCredentials("your-factorio-username", "your-factorio-token"),
	)

	// First request: Download data of the desired mod
	modRequest := ModRequest{
		ModName: "textplates",
	}
	modResponse, err := modRequest.CastResponse(client.Send(ctx, &modRequest))
	if err != nil {
		panic(err)
	}

	latestRelease := modResponse.SelectLatestRelease(nil) // Selects the latest release from the list of releases.

	// Second request: Actually download the mod file of the latest release
	downloadRequest := DownloadModRequest{
		DownloadUrl: latestRelease.DownloadUrl,
	}
	downloadResponse, err := downloadRequest.CastResponse(client.Send(ctx, &downloadRequest))
	if err != nil {
		panic(err)
	}

	// Do something with the received file content
	_ = os.WriteFile(latestRelease.FileName, downloadResponse.Content, 0644)
}
